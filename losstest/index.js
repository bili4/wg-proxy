//@ts-check
const REL_API_URL = 'https://speed.cloudflare.com';

const defaultConfig = {
    // Engine
    autoStart: true,

    // APIs
    downloadApiUrl: `${REL_API_URL}/__down`,
    uploadApiUrl: `${REL_API_URL}/__up`,
    logMeasurementApiUrl: null,
    logAimApiUrl: 'https://aim.cloudflare.com/__log',
    turnServerUri: 'turn.speed.cloudflare.com:50000',
    turnServerCredsApiUrl: `${REL_API_URL}/turn-creds`,
    turnServerUser: null,
    turnServerPass: null,
    rpkiInvalidHost: 'invalid.rpki.cloudflare.com',
    cfTraceUrl: `${REL_API_URL}/cdn-cgi/trace`,
    includeCredentials: false,

    // Measurements
    measurements: [
        { type: 'latency', numPackets: 1 }, // initial ttfb estimation
        { type: 'download', bytes: 1e5, count: 1, bypassMinDuration: true }, // initial download estimation
        { type: 'latency', numPackets: 20 },
        { type: 'download', bytes: 1e5, count: 9 },
        { type: 'download', bytes: 1e6, count: 8 },
        { type: 'upload', bytes: 1e5, count: 8 },
        {
            type: 'packetLoss',
            numPackets: 1e3,
            batchSize: 10,
            batchWaitTime: 10, // ms (in between batches)
            responsesWaitTime: 3000 // ms (silent time after last sent msg)
        },
        { type: 'upload', bytes: 1e6, count: 6 },
        { type: 'download', bytes: 1e7, count: 6 },
        { type: 'upload', bytes: 1e7, count: 4 },
        { type: 'download', bytes: 2.5e7, count: 4 },
        { type: 'upload', bytes: 2.5e7, count: 4 },
        { type: 'download', bytes: 1e8, count: 3 },
        { type: 'upload', bytes: 5e7, count: 3 },
        { type: 'download', bytes: 2.5e8, count: 2 }
    ],
    measureDownloadLoadedLatency: true,
    measureUploadLoadedLatency: true,
    loadedLatencyThrottle: 400, // ms in between loaded latency requests
    bandwidthFinishRequestDuration: 1000, // download/upload duration (ms) to reach for stopping further measurements
    estimatedServerTime: 10, // ms to discount from latency calculation (if not present in response headers)

    // Result interpretation
    latencyPercentile: 0.5, // Percentile used to calculate latency from a set of measurements
    bandwidthPercentile: 0.9, // Percentile used to calculate bandwidth from a set of measurements
    bandwidthMinRequestDuration: 10, // minimum duration (ms) to consider a measurement good enough to use in bandwidth calculation
    loadedRequestMinDuration: 250, // minimum duration (ms) of a request to consider it to be loading the connection
    loadedLatencyMaxPoints: 20 // number of data points to keep for loaded latency
};

class SelfWebRtcDataConnection {
    constructor({
        iceServers = [],
        acceptIceCandidate = candidate => {
            let protocol = candidate.protocol || '';
            // parsed webRTC candidate properties not extracted in Firefox: https://developer.mozilla.org/en-US/docs/Web/API/RTCIceCandidate
            if (!protocol && candidate.candidate) {
                const sdpAttrs = candidate.candidate.split(' ');
                sdpAttrs.length >= 3 && (protocol = sdpAttrs[2]);
            }
            return protocol.toLowerCase() === 'udp';
        },
        dataChannelCfg = {
            ordered: false,
            maxRetransmits: 0
        },
        ...rtcPeerConnectionCfg
    } = {}) {
        const sender = new RTCPeerConnection({
            iceServers,
            ...rtcPeerConnectionCfg
        });
        const receiver = new RTCPeerConnection({
            iceServers,
            ...rtcPeerConnectionCfg
        });

        let senderDc = sender.createDataChannel('channel', dataChannelCfg);
        senderDc.onopen = () => {
            this.#established = true;
            this.onOpen();
        };
        senderDc.onclose = () => this.close();
        // senderDc.onmessage = msg => this.#onMessage(msg.data);

        receiver.ondatachannel = e => {
            const dc = e.channel;
            dc.onclose = () => this.close();
            dc.onmessage = msg => this.onMessageReceived(msg.data);

            this.#receiverDc = dc;
        };

        sender.onconnectionstatechange = e => console.log('connection state change', e);
        sender.oniceconnectionstatechange = e => console.log('ice connection state change', e);
        sender.onicecandidateerror = e => console.log('ice error', e);
        sender.onicecandidate = e => {
            // console.log('sender', e.candidate);
            e.candidate &&
                acceptIceCandidate(e.candidate) &&
                receiver.addIceCandidate(e.candidate);
        };
        receiver.onicecandidate = e => {
            // console.log('receiver', e.candidate);
            e.candidate &&
                acceptIceCandidate(e.candidate) &&
                sender.addIceCandidate(e.candidate);
        };

        sender
            .createOffer()
            .then(offer => sender.setLocalDescription(offer))
            .then(() => receiver.setRemoteDescription(sender.localDescription))
            .then(() => receiver.createAnswer())
            .then(answer => receiver.setLocalDescription(answer))
            .then(() => sender.setRemoteDescription(receiver.localDescription));

        this.#sender = sender;
        this.#receiver = receiver;
        this.#senderDc = senderDc;
        this.#established = false;
    }

    // Public attributes
    onOpen = () => { }; // callback invoked when WebRTC TURN connection is established
    onClose = () => { }; // callback invoked when WebRTC TURN connection is closed
    onMessageReceived = () => { }; // callback invoked when a new message is received from the TURN server

    // Public methods
    send(msg) {
        return this.#senderDc.send(msg);
    }

    close() {
        this.#sender && this.#sender.close();
        this.#receiver && this.#receiver.close();
        this.#senderDc && this.#senderDc.close();
        this.#receiverDc && this.#receiverDc.close();

        this.#established && this.onClose();
        this.#established = false;
        return this;
    }

    // Internal state
    #established = false;

    #sender;
    #receiver;
    #senderDc;
    #receiverDc;
}

class PacketLossEngine {
    constructor({
        turnServerUri,
        turnServerCredsApi,
        turnServerCredsApiParser = ({ username, credential }) => ({
            turnServerUser: username,
            turnServerPass: credential
        }),
        turnServerCredsApiIncludeCredentials = false,
        turnServerUser,
        turnServerPass,
        numMsgs = 100,
        batchSize = 10,
        batchWaitTime = 10, // ms (in between batches)
        responsesWaitTime = 5000, // ms (debounced time after last msg without any response)
        connectionTimeout = 10000 // ms
    } = {}) {
        if (!turnServerUri) throw new Error('Missing turnServerUri argument');

        if ((!turnServerUser || !turnServerPass) && !turnServerCredsApi)
            throw new Error(
                'Missing either turnServerCredsApi or turnServerUser+turnServerPass arguments'
            );

        this.#numMsgs = numMsgs;

        (!turnServerUser || !turnServerPass
            ? // Get TURN credentials from API endpoint if not statically supplied
            fetch(turnServerCredsApi, {
                credentials: turnServerCredsApiIncludeCredentials
                    ? 'include'
                    : undefined
            })
                .then(r => r.json())
                .then(d => {
                    if (d.error) throw d.error;
                    return d;
                })
                .then(turnServerCredsApiParser)
            : Promise.resolve({
                turnServerUser,
                turnServerPass
            })
        )
            .catch(e => this.#onCredentialsFailure(e))
            .then(({ turnServerUser, turnServerPass }) => {
                const c = (this.#webRtcConnection = new SelfWebRtcDataConnection({
                    iceServers: [
                        {
                            urls: `turn:${turnServerUri}?transport=udp`,
                            username: turnServerUser,
                            credential: turnServerPass
                        }
                    ],
                    iceTransportPolicy: 'relay'
                }));

                let connectionSuccess = false;
                setTimeout(() => {
                    if (!connectionSuccess) {
                        c.close();
                        this.#onConnectionError('ICE connection timeout!');
                    }
                }, connectionTimeout);

                const msgTracker = this.#msgTracker;
                c.onOpen = () => {
                    connectionSuccess = true;

                    const self = this;
                    (function sendNum(n) {
                        if (n <= numMsgs) {
                            let i = n;
                            while (i <= Math.min(numMsgs, n + batchSize - 1)) {
                                msgTracker[i] = false;
                                c.send(i);
                                self.onMsgSent(i);
                                i++;
                            }
                            setTimeout(() => sendNum(i), batchWaitTime);
                        } else {
                            self.onAllMsgsSent(Object.keys(msgTracker).length);

                            const finishFn = () => {
                                c.close();
                                self.#onFinished(self.results);
                            };
                            let finishTimeout = setTimeout(finishFn, responsesWaitTime);

                            let missingMsgs = Object.values(self.#msgTracker).filter(
                                recv => !recv
                            ).length;
                            c.onMessageReceived = msg => {
                                clearTimeout(finishTimeout);

                                msgTracker[msg] = true;
                                self.onMsgReceived(msg);

                                missingMsgs--;
                                if (
                                    missingMsgs <= 0 &&
                                    Object.values(self.#msgTracker).every(recv => recv)
                                ) {
                                    // Last msg received, shortcut out
                                    finishFn();
                                } else {
                                    // restart timeout
                                    finishTimeout = setTimeout(finishFn, responsesWaitTime);
                                }
                            };
                        }
                    })(1);
                };
                c.onMessageReceived = msg => {
                    msgTracker[msg] = true;
                    this.onMsgReceived(msg);
                };
            })
            .catch(e => this.#onConnectionError(e.toString()));
    }

    // Public attributes
    #onCredentialsFailure = (e) => { console.error(e) }; // Invoked when unable to fetch TURN server credentials
    set onCredentialsFailure(f) {
        this.#onCredentialsFailure = f;
    }
    #onConnectionError = (e) => { console.error(e) }; // Invoked when unable to establish a connection with TURN server
    set onConnectionError(f) {
        this.#onConnectionError = f;
    }
    #onFinished = () => { }; // Invoked when the packet loss measurement is complete
    set onFinished(f) {
        this.#onFinished = f;
    }
    onMsgSent = () => { }; // Invoked when sending a new message to the TURN server
    onAllMsgsSent = () => { }; // Invoked when all messages have been sent
    onMsgReceived = () => { }; // Invoked when receiving a new message from the TURN server

    get results() {
        const totalMessages = this.#numMsgs;
        const numMessagesSent = Object.keys(this.#msgTracker).length;
        const lostMessages = Object.entries(this.#msgTracker)
            .filter(([, recv]) => !recv)
            .map(([n]) => +n);
        const packetLoss = lostMessages.length / numMessagesSent;
        return { totalMessages, numMessagesSent, packetLoss, lostMessages };
    }

    // Public methods

    // Internal state
    #msgTracker = {};
    #webRtcConnection;
    #numMsgs;
}
class PacketLossUnderLoadEngine extends PacketLossEngine {
    constructor({
        downloadChunkSize,
        uploadChunkSize,
        downloadApiUrl,
        uploadApiUrl,
        ...ptProps
    } = {}) {
        super(ptProps);

        if (downloadChunkSize || uploadChunkSize) {
            this.#loadEngine = new LoadNetworkEngine({
                download: downloadChunkSize
                    ? {
                        apiUrl: downloadApiUrl,
                        chunkSize: downloadChunkSize
                    }
                    : null,
                upload: uploadChunkSize
                    ? {
                        apiUrl: uploadApiUrl,
                        chunkSize: uploadChunkSize
                    }
                    : null
            });

            super.onCredentialsFailure =
                super.onConnectionError =
                super.onFinished =
                () => this.#loadEngine.stop();
        }
    }

    // Overridden attributes
    set qsParams(qsParams) {
        this.#loadEngine && (this.#loadEngine.qsParams = qsParams);
    }

    set fetchOptions(fetchOptions) {
        this.#loadEngine && (this.#loadEngine.fetchOptions = fetchOptions);
    }

    set onCredentialsFailure(onCredentialsFailure) {
        super.onCredentialsFailure = (...args) => {
            onCredentialsFailure(...args);
            this.#loadEngine && this.#loadEngine.stop();
        };
    }

    set onConnectionError(onConnectionError) {
        super.onConnectionError = (...args) => {
            onConnectionError(...args);
            this.#loadEngine && this.#loadEngine.stop();
        };
    }

    set onFinished(onFinished) {
        super.onFinished = (...args) => {
            onFinished(...args);
            this.#loadEngine && this.#loadEngine.stop();
        };
    }

    // Internal state
    #loadEngine;
}
const {
    turnServerUri,
    turnServerCredsApiUrl: turnServerCredsApi,
    turnServerUser,
    turnServerPass,
    includeCredentials,
    downloadApiUrl,
    uploadApiUrl,
} = defaultConfig;

const rangeElm = document.getElementById("number")

const { type, ...msmConfig } = defaultConfig.measurements[6];

rangeElm.value = msmConfig.numPackets


const preElm = document.getElementById("result")
/**@type {HTMLProgressElement} */
const progress = document.getElementById("progress")
/**@type {HTMLProgressElement} */
const recv_progress = document.getElementById("recv")
/**@type {HTMLCanvasElement} */
const cvElm = document.getElementById("draw")



progress.max = msmConfig.numPackets
recv_progress.max = msmConfig.numPackets

const ctx = cvElm.getContext("2d");

const rowNumber = 50
const gap = 2
const size = 8




function getPos(i) {
    i -= 1
    let hi = Math.floor(i / rowNumber)
    let wi = i % rowNumber
    return [wi * (size + 2 * gap) + gap, hi * (size + 2 * gap) + gap]
}


const startElm = document.getElementById("start")

startElm.onclick = () => {
    startElm.disabled = true
    document.body.classList.add('show')

    msmConfig.numPackets = rangeElm.value
    const { numPackets: numMsgs, ...ptCfg } = msmConfig;

    const cv_width = rowNumber * (size + 2 * gap)
    const cv_height = (size + 2 * gap) * Math.ceil(msmConfig.numPackets / rowNumber)
    cvElm.width = cv_width
    cvElm.height = cv_height

    ctx.fillStyle = '#efefef'
    ctx.fillRect(0, 0, cv_width, cv_height)
    ctx.fillStyle = '#ddd'
    for (let i = 1; i <= msmConfig.numPackets; i++) {
        const [x, y] = getPos(i)
        ctx.fillRect(x, y, size, size)
    }
    const engine = new PacketLossUnderLoadEngine({
        turnServerUri,
        turnServerCredsApi,
        turnServerCredsApiIncludeCredentials: includeCredentials,
        turnServerUser,
        turnServerPass,
        numMsgs,

        // if under load
        downloadChunkSize: undefined,
        uploadChunkSize: undefined,
        downloadApiUrl,
        uploadApiUrl,

        ...ptCfg
    })
    engine.onFinished = () => {
        console.log(engine.results)
        progress.value = msmConfig.numPackets
        recv_progress.value = (1 - engine.results.packetLoss) * msmConfig.numPackets
        preElm.textContent = JSON.stringify(engine.results)

        ctx.fillStyle = "#e65959"
        engine.results.lostMessages.forEach(i => {
            const [x, y] = getPos(i)
            ctx.fillRect(x, y, size, size)
        })
    }
    let sv = 0
    engine.onMsgSent = (i) => {
        sv++
        progress.value = sv
        ctx.fillStyle = "#aaa"
        const [x, y] = getPos(i)
        ctx.fillRect(x, y, size, size)
    }
    let rv = 0
    engine.onMsgReceived = (i) => {
        rv++
        recv_progress.value = rv
        ctx.fillStyle = "#69e659"
        const [x, y] = getPos(i)
        ctx.fillRect(x, y, size, size)
    }


}

