package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net/netip"
	"os"
	"strings"
	"time"

	"golang.zx2c4.com/wireguard/conn"
	"golang.zx2c4.com/wireguard/device"
	"golang.zx2c4.com/wireguard/tun"
	"golang.zx2c4.com/wireguard/tun/netstack"
)

const MaxWaitTime = 2 * time.Second

// echo -n abcd== | base64 -d | xxd -p -c32

func init() {
	log.SetFlags(log.Flags() | log.Llongfile)
}

type config struct {
	ipv4 string
	ipv6 string
	dns  string
	mtu  int

	wgstr   string
	verbose bool

	ipfname string
}

func initConfig() config {
	confname := flag.String("f", "warp.conf", "wireguard conf")
	fname := flag.String("ipf", "ip.txt", "ip to test")
	ips := flag.String("ip", "", "ipv4|ipv6|dns")
	mtu := flag.Int("mtu", 1420, "MTU")
	loglevel1 := flag.Bool("v", false, "log")

	flag.Parse()

	conf, err := os.OpenFile(*confname, os.O_RDONLY, os.ModePerm)
	if err != nil {
		panic(err)
	}

	confbytes, err := io.ReadAll(conf)
	if err != nil {
		panic(err)
	}
	confStr := string(confbytes)
	conf.Close()
	if len(*ips) <= 0 {
		fmt.Fprintln(os.Stderr, "need ip")
		os.Exit(1)
	}
	ipArr := strings.Split(*ips, "|")
	if len(ipArr) != 3 {
		fmt.Fprintln(os.Stderr, "need 3 ip")
		os.Exit(1)
	}
	return config{
		ipv4: ipArr[0],
		ipv6: ipArr[1],
		dns:  ipArr[2],
		mtu:  *mtu,

		wgstr:   confStr,
		verbose: *loglevel1,

		ipfname: *fname,
	}
}

type wg struct {
	tun  tun.Device
	tnet *netstack.Net
	dev  *device.Device
}

func (w wg) Close() {
	w.dev.Close()
	w.tun.Close()
}

func initWG(conf *config) wg {
	tun, tnet, err := netstack.CreateNetTUN(
		[]netip.Addr{netip.MustParseAddr(conf.ipv4), netip.MustParseAddr(conf.ipv6)},
		[]netip.Addr{netip.MustParseAddr(conf.dns)},
		conf.mtu)

	if err != nil {
		log.Panic(err)
	}

	var loglevel = device.LogLevelSilent
	if conf.verbose {
		loglevel = device.LogLevelVerbose
	}

	logger := device.NewLogger(loglevel, "")
	dev := device.NewDevice(tun, conn.NewDefaultBind(), logger)
	return wg{
		tun:  tun,
		tnet: tnet,
		dev:  dev,
	}
}

func main() {

	conf := initConfig()

	f, err := os.OpenFile(conf.ipfname, os.O_RDONLY, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	rd := bufio.NewReader(f)
	testAddr := netip.MustParseAddrPort("1.1.1.1:80")

	w := initWG(&conf)

	defer w.Close()
	err = w.dev.Up()
	for {
		if err != nil {
			log.Panic(err)
		}

		// dev.RemoveAllPeers()

		s, err := rd.ReadString('\n')
		if err == io.EOF {
			break
		}

		err = w.dev.IpcSet(conf.wgstr + "endpoint=" + s)

		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			continue
		}
		ctx, cancel := context.WithTimeout(context.Background(), MaxWaitTime)
		conn, err := w.tnet.DialContextTCPAddrPort(ctx, testAddr)
		cancel()
		if err != nil {
			continue
		}
		conn.Close()
		fmt.Println(strings.Trim(s, "\n"))

	}
	stats, err := w.dev.IpcGet()
	if err == nil {
		fmt.Fprintln(os.Stderr, stats)
	}
	w.dev.Down()
	os.Exit(0)
}
