package main

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/netip"
	"net/textproto"
	"os"
	"strings"
	"time"

	"golang.zx2c4.com/wireguard/conn"
	"golang.zx2c4.com/wireguard/device"
	"golang.zx2c4.com/wireguard/tun"
	"golang.zx2c4.com/wireguard/tun/netstack"
)

const MaxWaitTime = 1 * time.Second

var httpMapPort = "80"
var httpsMapPort = "443"

func init() {
	log.SetFlags(log.Flags() | log.Llongfile)
}

func handleHttpConnect(b []byte, tnet *netstack.Net, client net.Conn) (net.Conn, error) {
	rd1 := bytes.NewReader(b[:])

	rd := bufio.NewReader(rd1)

	tp := textproto.NewReader(rd)

	line, err := tp.ReadLine()

	if err != nil {

		return nil, err
	}

	tokens := strings.Split(line, " ")

	if len(tokens) < 3 {
		return nil, errors.New("tokens < 3")
	}

	method := tokens[0]
	host := tokens[1]
	proto := tokens[2]

	head, err := tp.ReadMIMEHeader()
	if err != nil {
		if err != io.EOF {

			return nil, err
		}
	} else {
		header := http.Header(head)
		host1 := header.Get("Host")
		if len(host1) > 0 {
			host = host1
		}
	}

	if !strings.Contains(host, ":") {
		host += ":" + httpMapPort
	} else {
		s := strings.Split(host, ":")
		host = s[0] + ":" + httpsMapPort
	}
	server, err := tnet.Dial("tcp", host)

	if err != nil {
		return nil, err
	}
	if method == "CONNECT" {
		fmt.Fprintf(client, "%s 200 Connection established\r\n\r\n", proto)
	} else {
		server.Write(b[:])
	}
	return server, nil
}

func handleSocks5Connect(tnet *netstack.Net, client net.Conn) (net.Conn, error) {

	_, err := client.Write([]byte{0x5, 0x0})
	if err != nil {
		return nil, err
	}
	var b [1024]byte
	n2, err2 := client.Read(b[:])
	err = err2
	if err != nil {
		return nil, err
	}

	var server net.Conn

	if n2 >= 4 {
		if b[0] == 0x5 && b[1] == 0x1 {

			if b[3] == 0x1 && n2 >= 10 {
				ip4 := (*[4]byte)(b[4:8])

				server, err = tnet.DialTCPAddrPort(netip.AddrPortFrom(netip.AddrFrom4(*ip4), binary.BigEndian.Uint16(b[8:10])))
				// tnet.Dial("tcp4", fmt.Sprintf("%d.%d.%d.%d:%d", b[4], b[5], b[6], b[7], binary.BigEndian.Uint16(b[8:10])))
				if err != nil {
					return nil, err
				}
				_, err = client.Write([]byte{0x05, 0x00, 0x00, 0x01, 0, 0, 0, 0, 0, 0})
				if err != nil {
					log.Println(err)
				}
			} else if b[3] == 0x4 && n2 >= 22 {
				ip6 := (*[16]byte)(b[4:20])

				server, err = tnet.DialTCPAddrPort(netip.AddrPortFrom(netip.AddrFrom16(*ip6), binary.BigEndian.Uint16(b[20:22])))
				//  tnet.Dial("tcp6", fmt.Sprintf("[%s]:%d", printAAAAResource(ip6), binary.BigEndian.Uint16(b[20:22])))
				if err != nil {
					return nil, err
				}
				_, err = client.Write([]byte{0x05, 0x00, 0x00, 0x04, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
				if err != nil {
					log.Println(err)
				}
			} else if b[3] == 0x3 {
				host := string(b[5 : 5+b[4]])
				port := binary.BigEndian.Uint16(b[5+b[4] : 7+b[4]])

				server, err = tnet.Dial("tcp", fmt.Sprint(host, ":", port))
				if err != nil {

					return nil, err
				}

				_, err = client.Write([]byte{0x05, 0x00, 0x00, 0x03, 0, 0, 0, 0, 0, 0})
				if err != nil {

					return nil, err
				}
			} else {
				err = errors.New("invalid format")
			}
		} else {
			err = errors.New("unsupported format")
		}
	} else {
		err = errors.New("unsupported format")
	}
	return server, err
}

func handleProxy(client net.Conn, tnet *netstack.Net) {

	defer client.Close()

	var b [1420]byte

	n, err := client.Read(b[:])
	if err != nil {
		log.Println(err)
		return
	}
	var server net.Conn
	if n > 2 && b[0] == 0x5 {
		server, err = handleSocks5Connect(tnet, client)
	} else {
		server, err = handleHttpConnect(b[:n], tnet, client)
	}
	if err != nil {
		log.Println(err)
		return
	}
	defer server.Close()

	go io.Copy(writerOnly{server}, client)
	io.Copy(writerOnly{client}, server)

}

type writerOnly struct {
	io.Writer
}

type config struct {
	ipv4 string
	ipv6 string
	dns  string
	mtu  int

	wgstr   string
	verbose bool

	laddr string
}

func initConfig() config {
	confname := flag.String("f", "wg0.conf", "wireguard conf")
	addr := flag.String("l", ":8000", "listen address")
	ips := flag.String("ip", "", "ipv4|ipv6|dns")
	mtu := flag.Int("mtu", 1420, "MTU")
	loglevel1 := flag.Bool("v", false, "log")

	flag.Parse()

	conf, err := os.OpenFile(*confname, os.O_RDONLY, os.ModePerm)
	if err != nil {
		panic(err)
	}

	confbytes, err := io.ReadAll(conf)
	if err != nil {
		panic(err)
	}
	confStr := string(confbytes)
	conf.Close()
	if len(*ips) <= 0 {
		fmt.Fprintln(os.Stderr, "need ip")
		os.Exit(1)
	}
	ipArr := strings.Split(*ips, "|")
	if len(ipArr) != 3 {
		fmt.Fprintln(os.Stderr, "need 3 ip")
		os.Exit(1)
	}
	return config{
		ipv4: ipArr[0],
		ipv6: ipArr[1],
		dns:  ipArr[2],
		mtu:  *mtu,

		wgstr:   confStr,
		verbose: *loglevel1,

		laddr: *addr,
	}
}

type wg struct {
	tun  tun.Device
	tnet *netstack.Net
	dev  *device.Device
}

func (w wg) Close() {
	w.dev.Close()
	w.tun.Close()
}

func initWG(conf *config) wg {
	tun, tnet, err := netstack.CreateNetTUN(
		[]netip.Addr{netip.MustParseAddr(conf.ipv4), netip.MustParseAddr(conf.ipv6)},
		[]netip.Addr{netip.MustParseAddr(conf.dns)},
		conf.mtu)

	if err != nil {
		log.Panic(err)
	}

	var loglevel = device.LogLevelSilent
	if conf.verbose {
		loglevel = device.LogLevelVerbose
	}

	logger := device.NewLogger(loglevel, "")
	dev := device.NewDevice(tun, conn.NewDefaultBind(), logger)
	return wg{
		tun:  tun,
		tnet: tnet,
		dev:  dev,
	}
}

func main() {

	conf := initConfig()

	l, err := net.Listen("tcp", conf.laddr)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	w := initWG(&conf)

	defer w.Close()

	err = w.dev.Up()
	if err != nil {
		log.Panic(err)
	}

	err = w.dev.IpcSet(conf.wgstr)
	if err != nil {
		log.Panic(err)
	}

	for {
		c, err := l.Accept()
		if err != nil {
			break
		}
		go handleProxy(c, w.tnet)
	}
	w.dev.Down()

}
