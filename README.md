# WG proxy

## main

通过HTTP或SOCKS5代理使用wireguard网络

## detect

测量可用IP的丢包率和延迟

## losstest

使用@cloudflare/speedtest包在浏览器中测试数据包丢失率

```bash
cd losstest
python -m http.server
```

## wg0.conf 配置文件

具体可以查看wireguard官方资料

```bash
echo -n $base64_key | base64 -d | xxd -p -c32
```

```txt
private_key=
public_key=
allowed_ip=
allowed_ip=

```